import os
import json

def read_json(filename):
  with open(filename, 'r', encoding="utf-8") as f:
    return json.load(f)

def to_absolute_time(timestr, zero_date):
  # 0:00:14
  parts = timestr.split(":")
  epoch = int(parts[0]) * 3600000 + int(parts[1]) * 60000 + int(parts[2]) * 1000
  return epoch + zero_date

def load_button_data(button_data):
  # TODO read these from data
  keys = [
    '2021-06-30 Gabbi Scotto Audio.m4a',
    'Lara-GMT20210701-165830_Recording.m4a',
    'Ish-GMT20210701-195749_Recording.m4a',
    '2021-07-06 - James - Audio.m4a',
    '2021-07-01 - Amy Burkhart - VW Owner - Audio.m4a',
    'Michael-GMT20210701-183145_Recording.m4a',
    '2021-06-30 Rudy Rosefort.m4a'
  ]
  for key in keys:
    button_data[key] = read_json(key + '___button.json')
  return

def find_button(transcript, button_data, zero_date):
  start = to_absolute_time(transcript["In"], zero_date)
  end = to_absolute_time(transcript["Out"], zero_date)
  for b in button_data:
    if (start <= b[0] and end >= b[0]):
      return b 
  return

def merge_button_to_transcript(transcript, button_data):
  ret = transcript.copy()
  zero_date = button_data["recording_start"]
  button_click = find_button(ret, button_data["data"], zero_date)
  if (not(button_click is None)):
      if ("Button" in ret):
        ret["Button"].append(button_click[1])
      else:
        ret["Button"] = [button_click[1]]

  return ret

def merge_button_data(transcripts, button_data_cache):
  ret = []
  for t in transcripts:
    button_data = button_data_cache[t["Source"]]
    transcript = merge_button_to_transcript(t, button_data)
    ret.append(transcript)
  return ret

def main():
  button_data_cache = {}
  load_button_data(button_data_cache)
  transcripts = read_json("../web_prototype/json/transcript.json")
  transcripts["transcript"] = merge_button_data(transcripts["transcript"], button_data_cache)
  with open('./transcript-button-merged.json', 'w', encoding='utf-8') as f:
      json.dump(transcripts, f, ensure_ascii=False, indent=4)

main()