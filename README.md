# NLP Tools for Qually

## Intro and setup

### Requirements

- Python 3.6
- It is recommended to create a python virtualenv
- [spaCy 3.0](https://spacy.io/) `pip install spacy`
- pip install python-slugify

## Training a Spacy Texcat model

This is based on [Building a Text Classifier with Spacy 3.0](https://medium.com/analytics-vidhya/building-a-text-classifier-with-spacy-3-0-dd16e9979a).

The spaCy `en_core_web_lg` model, which contains word vectors, will be required for training:

`python -m spacy download en_core_web_lg`

The `create_data` script will generate binary spaCy Doc objects based on the training data in `./data`

To output a trained pipeline:
`python -m spacy train ./config/config.cfg --paths.train ./bin/train.spacy --paths.dev ./bin/train.spacy --output ./bin/output`
(This is temporary until the script properly builds a separate dev model)

To use that pipeline, interactively in the shell use the `test_textcat` script.

There is A LOT of improvements and fine-tuning that needs to happen to make this work properly for Qually.

At this point, this is just plumbing rather than actually NLP.
