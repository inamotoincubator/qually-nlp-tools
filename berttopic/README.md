This is a simple demo of the BERTopic library using our existing interviews.

https://maartengr.github.io/BERTopic/

To install, just run:

`pip install bertopic`

then run

`python summarize_topics.py`

On a Mac Mini M1 it typically takes 2-3 minutes for this script to run.
