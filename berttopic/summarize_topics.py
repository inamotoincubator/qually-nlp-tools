# This script is a quick demo of how to generate topics using BERTopic

from bertopic import BERTopic
import csv 

rows=[]
data = { 'text': ''}

# This takes our CSV file, merges Speaker 1 and Speaker 2
# and places the resulting text snippets into a single array
with open('./manual_tagging.csv', 'r', encoding="utf-8") as raw_csv_file:
    csvreader = csv.reader(raw_csv_file)
    for row in csvreader:
        if (row[0] == "Speaker 1" and data['text'].strip() != ""):
            rows.append(data["text"])
            data = { 'text': ''}
        data['text'] = data['text'] + row[1] + ' '
        

# This is all the code to train!
# just create a model, and pass in our array of text
model = BERTopic(language="english", calculate_probabilities=True, verbose=True, nr_topics=8)
topics, probabilities = model.fit_transform(rows)


# Now the model has been generated, so these are just some utilities to display the results
choices = model.get_topics()
topic_names={}

for key in choices:
    if (key >= 0):
        tags = []
        for t in choices[key]:
            tags.append(t[0])
        summary_name = ("-").join(tags)
        topic_names[key] = summary_name
        print(summary_name) 
        print(tags)
        print("---")
    else:
        topic_names[key] = "(common words)"


print("Topics similar to 'inspiration'")
topic_ids, topic_similarity = model.find_topics("inspiration")

for idx, val in enumerate(topic_ids):
    print (str(topic_similarity[idx]) + " : " + topic_names[val] )




# Earlier, I was playing around with other embedding_models, and this is some of the code from that
#
# import spacy
# from flair.embeddings import TransformerDocumentEmbeddings
# model = BERTopic(embedding_model=nlp, language="english", calculate_probabilities=True, verbose=True)
# roberta = TransformerDocumentEmbeddings('roberta-base')
# nlp = spacy.load("en_core_web_trf", exclude=['tagger', 'parser', 'ner', 'attribute_ruler', 'lemmatizer'])