import { useHistory } from "react-router-dom";
import Tags from "./Tags";
import { ITopic } from "./types";
import "./Topics.css";

function Topics(props: { topics: ITopic[] }) {
  const history = useHistory();
  return (
    <div className="topics">
      <a href="/">&laquo; Back</a>
      <div className="list">
        {props.topics.map((topic) => (
          <Topic
            key={topic.id}
            topic={topic}
            onSelect={() => history.push(`/topics/${topic.id}`)}
          />
        ))}
      </div>
    </div>
  );
}

function Topic(props: { topic: ITopic; onSelect: () => void }) {
  return (
    <div className="topic" onClick={props.onSelect}>
      <div className="left">
        <div className="name">{props.topic.tags[0]}</div>
        <Tags tags={props.topic.tags} />
      </div>
      <div className="right">
        <div className="count">{props.topic.count}</div>
      </div>
    </div>
  );
}

export default Topics;
