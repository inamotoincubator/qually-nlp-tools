export interface ITopic {
  id: string;
  name: string;
  tags: string[];
  topic_num: number;
  count: number;
  conversations: string[];
}

export interface ISentimentSentence {
  text: string;
  sentiment_score: number;
  sentiment_magnitude: number;
  id: string;
}

export interface ISentiment {
  sentiment_score: number;
  sentiment_magnitude: number;
  positive_sentences: ISentimentSentence[];
  negative_sentences: ISentimentSentence[];
}

export interface IConversation {
  id: string;
  chat: string;
  topics: string[];
  sentiment: ISentiment;
}
