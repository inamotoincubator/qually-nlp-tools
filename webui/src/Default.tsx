import { useCallback, useEffect, useRef } from "react";

export default function Default(props: { onData: (data: any) => void }) {
  const ref = useRef<HTMLInputElement>(null);
  const onData = useCallback((data) => props.onData(data), [props])
  useEffect(() => {
    if (ref === null || ref.current == null) return;
    const input = ref.current;
    const listener = (e: any) => {
      const fileList = e.target.files;
      const reader = new FileReader();
      reader.addEventListener("load", (e: any) => {
        onData(JSON.parse(e.target.result));
      });
      reader.readAsText(fileList[0]);
    };
    input.addEventListener("change", listener);
    return () => {
      input.removeEventListener("change", listener);
    };
  }, [onData]);
  return <input type="file" ref={ref} />
}