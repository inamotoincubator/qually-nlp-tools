import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Topics from "./Topics";
import Conversations from "./Conversations";
import { IConversation, ITopic } from "./types";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";

function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}

function App(props: { data: { topics: ITopic[], conversation: IConversation[] } }) {
  const topics = props.data.topics;
  const conversations = props.data.conversation;
  return (
    <Router>
      <ScrollToTop />
      <div className="App">
        <Switch>
          <Route
            path="/topics/:topicId"
            render={(renderProps) => {
              const topicId = renderProps.match.params.topicId
              const convs = conversations.filter(c => c.topics.indexOf(topicId) >= 0)
              const topic = topics.filter(t => t.id === topicId)[0]
              return <Conversations topic={topic} conversations={convs} />
            }}
          ></Route>
          <Route path="/topics">
            <Topics topics={topics} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
