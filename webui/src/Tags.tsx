import "./Tags.css";

export default function Tags(props: { tags: string[] }) {
  return (
    <ul className="tags">
      {props.tags.map((tag) => (
        <li key={tag}>
          <button className="tag">{tag}</button>
        </li>
      ))}
    </ul>
  );
}
