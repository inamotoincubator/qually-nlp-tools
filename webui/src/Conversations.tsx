import { Link } from "react-router-dom";
import Tags from "./Tags";
import { IConversation, ISentiment, ISentimentSentence, ITopic } from "./types";
import "./Conversations.css";
import { useState } from "react";

const SORT_ORDER = [
  { key: "", label: "(Choose one)" },
  { key: "sentiment_score", label: "Score" },
  { key: "sentiment_magnitude", label: "Magnitude" },
];

function Conversations(props: {
  topic: ITopic;
  conversations: IConversation[];
}) {
  const [sortOrder, setSortOrder] = useState<number>(0);
  const conversations =
    sortOrder > 0
      ? props.conversations.slice(0).sort(
          (a, b) =>
            // @ts-ignore
            Math.abs(b.sentiment[SORT_ORDER[sortOrder].key]) -
            // @ts-ignore
            Math.abs(a.sentiment[SORT_ORDER[sortOrder].key])
        )
      : props.conversations;
  return (
    <div className="conversations">
      <div className="backlink">
        <Link to="/topics">&laquo; Back</Link>
      </div>
      <header>
        <h2>{props.topic.tags[0]}</h2>
        <label htmlFor="sort-order">
          Sort order:
          <select
            id="sort-order"
            onChange={(e) => setSortOrder(e.currentTarget.selectedIndex)}
            defaultValue={sortOrder}
          >
            {SORT_ORDER.map((order) => (
              <option key={order.key} value={order.key}>
                {order.label}
              </option>
            ))}
          </select>
        </label>
      </header>
      <div className="list">
        {conversations.map((c) => (
          <Conversation topic={props.topic} conversation={c} key={c.id} />
        ))}
      </div>
    </div>
  );
}

function Conversation(props: { topic: ITopic; conversation: IConversation }) {
  const sentiment = props.conversation.sentiment;
  const borderStyles = {
    borderColor:
      props.conversation.sentiment.sentiment_score < 0
        ? `rgb(${255 * -sentiment.sentiment_score}, 0, 0)`
        : `rgb(0, ${255 * sentiment.sentiment_score}, 0)`,
    borderWidth: sentiment.sentiment_magnitude + 1,
  };
  const chat = props.conversation.chat;
  const tags = props.topic.tags.reduce<string[]>((c, v) => {
    if (chat.indexOf(v) >= 0) c.push(v);
    return c;
  }, []);
  return (
    <div
      className="conversation"
      style={borderStyles}
      title={`score: ${sentiment.sentiment_score}, magnitude: ${sentiment.sentiment_magnitude}`}
    >
      <div className="name">{props.conversation.id}</div>
      <Chat chat={chat} sentiment={sentiment} />
      <Tags tags={tags} />
    </div>
  );
}

function Chat(props: { chat: string; sentiment: ISentiment }) {
  const processChat = (s: ISentimentSentence) => {
    const start = chatHtml.indexOf(s.text);
    const a = chatHtml.slice(0, start);
    const c = chatHtml.slice(start + s.text.length);
    const b = sentimentSpan(s);
    chatHtml = a + b + c;
  };
  let chatHtml = props.chat;
  props.sentiment.positive_sentences.forEach(processChat);
  props.sentiment.negative_sentences.forEach(processChat);
  return (
    <div className="chat" dangerouslySetInnerHTML={{ __html: chatHtml }} />
  );
}

function sentimentSpan(s: ISentimentSentence) {
  const styles = `background-color: ${
    s.sentiment_score < 0
      ? `rgba(${255 * -s.sentiment_score}, 0, 0, ${
          1 - s.sentiment_magnitude / 10
        }); color: #FFF`
      : `rgba(0, ${255 * s.sentiment_score}, 0, ${
          1 - s.sentiment_magnitude / 10
        }); color: #333`
  }; border-radius: 8px; padding: 8px;`;
  return `<span style="${styles}" title="score: ${s.sentiment_score}, magnitude: ${s.sentiment_magnitude}">${s.text}</span>`;
}

export default Conversations;
