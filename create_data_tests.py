import csv
import os
import spacy
from spacy.tokens import DocBin
from slugify import slugify

nlp = spacy.load("en_core_web_md")
default_filepath = 'data/trint'

def read_data_rows(dir=default_filepath):
    rows = []
    row_len = 0
    file_counter = 0
    for filename in os.listdir(dir):
        f = os.path.join(dir, filename)
        file_counter = file_counter + 1
        with open(f, 'r', encoding="utf-8") as raw_csv_file:
            csvreader = csv.reader(raw_csv_file)
            for row in csvreader:
                data = { 'id': file_counter, 'text': row[3], 'speaker': row[4], 'tags': set()}
                for tagnum in range(6, len(row), 2):
                    if row[tagnum]:
                        data['tags'].add(row[tagnum])
                rows.append(data)
    return rows


# TODO - write a function that clusters Speaker 1 and 2 together
def cluster_rows(data=[]):
    return data


def rows_to_nlp_pipe(data=[], tag=""):
    not_tag = ("NOT " + tag)
    docs = []
    text_data = []
    for row in data:
        text_data.append([row['text'], tag in row['tags']])

    for doc, label in nlp.pipe(text_data, as_tuples=True):
        if label:
            doc.cats[tag] = 1
        else:
            doc.cats[not_tag] = 1
        docs.append(doc)
    return docs


def create_single_file_test(data=[], id=1, prefix="test"):
    doc_rows = []
    for row in data:
        if row['id'] == id:
            doc_rows.append(row)
    
    tags = set()
    for row in doc_rows:
        for tag in row['tags']:
            tags.add(tag)

    for tag in tags:
        training_rows = doc_rows[:len(doc_rows)//2]
        valid_rows = doc_rows[len(doc_rows)//2:]

        training_docs = rows_to_nlp_pipe(data=training_rows, tag=tag)
        valid_docs = rows_to_nlp_pipe(data=valid_rows, tag=tag)

        test_name = slugify(str(id)) + "-" + slugify(tag)

        doc_bin = DocBin(docs=training_docs)
        doc_bin.to_disk("./bin/" + test_name + "-train.spacy")
        doc_bin = DocBin(docs=valid_docs)
        doc_bin.to_disk("./bin/" + test_name + "-valid.spacy")



# TODO - create doc sets by randomizing across all the files
def create_random_test(data=[]):
    print("hi")    


raw_data = read_data_rows()
# clustered_data = cluster_rows(data=raw_data)

create_single_file_test(data=raw_data, id=1, prefix="test1")
create_single_file_test(data=raw_data, id=2, prefix="test2")

