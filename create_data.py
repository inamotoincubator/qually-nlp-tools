import spacy
import csv
import os
from spacy.tokens import DocBin

nlp = spacy.load("en_core_web_md")


def make_docs(data):
    docs = []
    pb_count = 0
    npb_count = 0
    for doc, label in nlp.pipe(data, as_tuples=True):
        if label:
            doc.cats['PURCHASE_BEHAVIOR'] = 1
            pb_count = pb_count + 1
        else:
            doc.cats['NOT_PURCHASE_BEHAVIOR'] = 1
            npb_count = npb_count + 1
        docs.append(doc)
    print(f'Created {len(docs)} docs for training.')
    print(f'Purchase behavior docs flagged: {pb_count}')
    print(f'Non purchase behavior docs flagged: {npb_count}')
    return docs


def create_data(dir):
    chunks = []

    for filename in os.listdir(dir):
        f = os.path.join(dir, filename)

        with open(f, 'r', encoding="utf-8") as raw_csv_file:
            csvreader = csv.reader(raw_csv_file)

            for row in csvreader:
                row_data = []
                doc = nlp(row[3])
                if len(doc) < 5:
                    continue
                row_data.append(doc.text)
                label = 0
                for token in doc:
                    if token.lemma_ in ('buy', 'purchase', 'subscribe'):
                        label = 1
                        break
                        # TODO: Handle multiple labels.
                row_data.append(label)
                chunks.append(row_data)
    return chunks


training_docs = make_docs(create_data('./data/transcripts/train'))
doc_bin = DocBin(docs=training_docs)
doc_bin.to_disk("./bin/train.spacy")

dev_docs = make_docs(create_data('./data/transcripts/dev'))
doc_bin = DocBin(docs=training_docs)
doc_bin.to_disk("./bin/dev.spacy")


# Write training data to disk
