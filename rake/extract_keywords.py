import RAKE
from bertopic import BERTopic

# config
stopwords_filename = 'SmartStoplist.txt'
guide_filename = 'vw-interview-guide-qs-only.txt'

# minimum number of characters to be considered a keyword
config_minChars = 1

# maximum number of words in a keyword/phrase
config_maxWords = 3

# minimum frequency for a word to be considered a keyword
config_minFreq = 1

# minimum score for consideration as a valuable keywords
config_minScore = 4

# load stopwords
Rake = RAKE.Rake(stopwords_filename)

# run extraction
keywords = []
with open(guide_filename) as f:
    content = f.read()
    keywords = Rake.run(content, minCharacters=config_minChars,
                        maxWords=config_maxWords, minFrequency=config_minFreq)

# match against model and print
model = BERTopic.load('../web_prototype/models/model_10_10')
for i in range(len(keywords)):
    if keywords[i][1] >= config_minScore:
        print(f'Searching model for: {keywords[i][0]}')
        similar_topics = model.find_topics(keywords[i][0], top_n=1)
        print(model.get_topic(similar_topics[0][0]))
