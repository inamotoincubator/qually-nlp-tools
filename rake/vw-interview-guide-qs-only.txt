Reasons for owning a car and attitudes on ownership
What car(s) do you own right now?
Why did you buy a car?
What are the high points & low points of owning a car?
What other brands have you owned in the past?
Value in your life, role in your life, emotions associated with owning and driving
If you could describe your car with one word, what would it be? E.g., Pet, Tool, Appliance, Friend
When you think about your car in the context of other things you've bought and own, what do you associate it with?
Would you consider your car a very important thing you own or not?
Is it a highly 'functional' thing in your life, or do you view it a different way?
How do you talk about your car with other people?
Does your car have a name?
Can you recall the moment when you felt like your car was 'your car'?
What was their last car purchase like? What were memorable moments?
Where did you start? What was your process like?
How did you pick the top brands you wanted?
How did rationality versus emotionality play a role in the car buying process?
Do you think the kind of car a person drives says something about them as a person? What are some examples of things you thought?
Why did you choose VW?
What does (quality, price, value, environmentally friendly... etc) mean to you?
Are these the same things you look for when making other purchases, both big and small?
What do you think a VW says about you to other people? What does it mean to you?
Why did you choose your VW model?
Are you proud to drive your VW? Why or why not?
Can you think of a time when you were particularly proud to talk about your VW?
What is your favorite thing about the VW brand? Low points?
If you were to think beyond the vehicles, what comes to mind when you think about VW?
Are their personal experiences you've had that created these associations?
Can you describe the quintessential "VW Person"?
What do they care about?
What are their hobbies?
Will they keep driving VW? Why or why not?
Do you identify as a quintessential "VW Person"? Why or why not?
Can you think back to when you first became aware of the VW brand?
When was it?
How did you feel about the brand?
Do you feel differently about the brand now? Why or why not?
Do you feel like you're part of a VW community (not a physical one, but one made up of like-minded drivers?)
Do you feel like driving a VW means you're part of a larger movement?
Sustainable
In the context of a car, what does sustainable mean to you?
In the context of VW specifically, what does sustainable mean to you?
Deserves a Better Car
How do you feel when someone tells you that you "deserve" something?
What is a "better car"?
What if someone said to you "everyone deserves a better car"?
Is this a sentiment you could associate with VW? Why?
Is this a sentiment you would repeat when you talk about VW? Why?
If someone said to you, "you deserve a better car," how would you feel? How would you react (what would your response to that person be?)
