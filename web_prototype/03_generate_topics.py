
from bertopic import BERTopic
import json

files = ['./models/model_6_5','./models/model_8_5','./models/model_8_10','./models/model_10_5','./models/model_10_10']
data = []

for f in files:
    model_name = f.split("/")[-1]
    model = BERTopic.load(f)
    choices = model.get_topics()
    model_data = []
    for key in choices:
        if (key >= 0):
            tags = []
            for t in choices[key]:
                tags.append(t[0])
            summary_name = ("-").join(tags)
            model_data.append({"name": summary_name, "tags": tags, "topic_num": key})

    data.append( {'id': model_name, 'topics': model_data})

with open('./json/topics.json', 'w', encoding='utf-8') as f:
    json.dump(data, f, ensure_ascii=False, indent=4)

