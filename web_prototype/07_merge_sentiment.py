
import json 


def positive_sentences(sentences=[]):
    result = []
    for row in sentences:
        if (row["sentiment_score"] > 0.7):
            result.append(row)
    return result

def negative_sentences(sentences=[]):
    result = []
    for row in sentences:
        if (row["sentiment_score"] < -0.7):
            result.append(row)
    return result


files = ['./json/model_6_5','./json/model_8_5','./json/model_8_10','./json/model_10_5','./json/model_10_10']

sentiment = {}
positives = []
negatives = []

with open('./json/sentiment.json') as f:
    data = json.load(f)
    for row in data:
        s = row['sentiment']
        sentiment[row['id']] = {
            "sentiment_score": s['sentiment_score'],
            "sentiment_magnitude": s['sentiment_magnitude'],
            "positive_sentences": positive_sentences(s['sentences']),
            "negative_sentences": negative_sentences(s['sentences'])
        }
        for i in sentiment[row['id']]["positive_sentences"]:
            i['id'] = row['id']
            positives.append(i)
        for i in sentiment[row['id']]["negative_sentences"]:
            i['id'] = row['id']
            negatives.append(i)

positives.sort(key=lambda x: x["sentiment_score"], reverse=True)
negatives.sort(key=lambda x: x["sentiment_score"])

for f in files:
    with open(f + ".json") as jsonfile:
        data = json.load(jsonfile)

        for id in data["conversation_lookup"]:
            idx = data["conversation_lookup"][id]
            data["conversation"][idx]["sentiment"] = sentiment[id]

        data["sentiment"] = {
            "most_positive_sentences": positives[:25],
            "most_negative_sentences": negatives[:25]
        }

        with open(f + '-merged.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)
