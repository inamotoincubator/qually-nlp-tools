import json

f1 = open('./json/conversation-topics.json')
conversations = json.load(f1)

f2 = open('./json/topics.json')
models = json.load(f2)

for model in models:

    id = model['id']

    conversation = []
    conversation_lookup = {}
    for idx, convo in enumerate(conversations):
        conversation_lookup[convo['id']] = idx 
        conversation.append({'id': convo['id'], 'chat': convo['chat'], 'topics': []})

    result = {'id': id, 'conversation': conversation, 'conversation_lookup': conversation_lookup, 'conversation_count': len(conversations), 'topics':[], 'topic_lookup': {}, 'topic_count': 0}
    
    for idx, topic in enumerate(model['topics']):
        # count the instance of this topic in conversations 
        count = 0
        convo_ids = []
        for convo in conversations:
            for convotopic in convo['topics']:
                if convotopic['model'] == id:

                    # only choose the first topic, don't loop through all of them
                    if len(convotopic['topics']) > 0:
                        if convotopic['topics'][0][0] == topic['topic_num']:
                            count = count + 1
                            convo_ids.append(convo['id'])
                            lookup = conversation_lookup[convo['id']]
                            result['conversation'][lookup]['topics'].append(topic['name'])

        tagnames = (' ').join(map(lambda x:x.capitalize(), topic['tags']))

        obj = {'id': topic['name'], 'name': tagnames, 'tags': topic['tags'], 'topic_num':topic['topic_num'], 'count': count, 'conversations': convo_ids}

        result['topic_lookup'][topic['name']] = idx
        result['topics'].append(obj)


    result['topic_count'] = len(result['topics'])

    with open('./json/' + id + '.json', 'w', encoding='utf-8') as f:
        json.dump(result, f, ensure_ascii=False, indent=4)




