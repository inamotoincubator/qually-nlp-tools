
from bertopic import BERTopic
import csv 
import json

with open('./json/conversation.json') as f:
    data = json.load(f)
    conversation = data['conversation']
    files = ['./models/model_6_5','./models/model_8_5','./models/model_8_10','./models/model_10_5','./models/model_10_10']

    for f in files:
        model_name = f.split("/")[-1]
        model = BERTopic.load(f)
        
        for idx in range(len(conversation)):
            print(idx)
            topic_obj = {'model': model_name, 'topics': []}
            topic_ids, topic_similarity = model.find_topics(conversation[idx]['chat'])

            for idx2, val in enumerate(topic_ids):
                if (topic_ids[idx2] > -1 and topic_similarity[idx2] > 0.1):
                    topic_obj['topics'].append((topic_ids[idx2], topic_similarity[idx2]))
                    
            if ("topics" in conversation[idx]):
                conversation[idx]["topics"].append(topic_obj)
            else:
                conversation[idx]["topics"] = [topic_obj]


    with open('./json/conversation-topics.json', 'w', encoding='utf-8') as f:
        json.dump(conversation, f, ensure_ascii=False, indent=4)



