import os
import csv
import json

def read_data_from_fumiaki():
    dir = './raw_data'
    rows = []
    row_len = 0
    file_counter = 0
    for filename in os.listdir(dir):
        if ".tsv" in filename:
            interview_name = filename.split(".")[0].lower()
            f = os.path.join(dir, filename)
            with open(f, 'r', encoding="utf-8") as raw_csv_file:
                csvreader = csv.DictReader(raw_csv_file, delimiter='\t')
                for row in csvreader:
                    row['line'] = csvreader.line_num
                    row['filename'] = interview_name
                    tagset = set()
                    if None in row:
                        tags = row[None]
                        for tagnum in range(0, len(tags), 2):
                            if (tags[tagnum]):
                                tagset.add(tags[tagnum])
                        del row[None]
                    row['tags'] = list(tagset)
                    rows.append(row)
                    
    return rows

def read_data_from_tagged_file():
    rows = []
    with open('./raw_data/TAGGED.tsv', 'r', encoding="utf-8") as raw_csv_file:
        csvreader = csv.DictReader(raw_csv_file, delimiter='\t')
        for row in csvreader:
            row['line'] = csvreader.line_num
            row['filename'] = row['Source'].split(".")[0].lower()
            rows.append(row)
            
    return rows


def make_data_lookup(data=[]): 
    lookup ={}
    for idx, row in enumerate(data):
        name = row['filename'] + "-" + str(row['line'])
        lookup[name] = idx
    return lookup


def make_conversations(data=[]):
    rows = []
    chat = ''
    tags = set()
    id_counter = 1
    for row in data:
        if (row['Speaker'] == "Speaker 1" and chat.strip() != ""):
            rows.append({'id': row['filename'] + "-" + str(id_counter), 'chat': chat.strip(), 'tags': list(tags)})
            id_counter = (id_counter + 1)
            chat = ''
            tags = set()
        
        if ('Text' in row):
            chat = chat + row['Text'] + ' '
        if ('tags' in row):
            for tag in row['tags']:
                tags.add(tag)
        if ('TAG' in row):
            if (row['TAG'] != ""):
                tags.add(row['TAG'])

    return rows

def make_conversation_lookup(data=[]): 
    lookup ={}
    for idx, row in enumerate(data):
        lookup[row['id']] = idx
    return lookup


transcript = {}
# data['transcript'] = read_data_from_fumiaki()
transcript['transcript'] = read_data_from_tagged_file()
transcript['transscript_lookup'] = make_data_lookup(transcript['transcript'])
with open('./json/transcript.json', 'w', encoding='utf-8') as f:
    json.dump(transcript, f, ensure_ascii=False, indent=4)

conversation = {}
conversation['conversation'] = make_conversations(transcript['transcript'])
conversation['conversation_lookup'] = make_conversation_lookup(conversation['conversation'])
with open('./json/conversation.json', 'w', encoding='utf-8') as f:
    json.dump(conversation, f, ensure_ascii=False, indent=4)

