
from bertopic import BERTopic
import csv 
import json

rows = []
with open('./json/conversation.json') as f:
    data = json.load(f)
    for row in data["conversation"]:
        rows.append(row['chat'])


# This is all the code to train!
# just create a model, and pass in our array of text
model1 = BERTopic(language="english", calculate_probabilities=True, verbose=True, nr_topics=6, top_n_words=5)
topics1, probabilities1 = model1.fit_transform(rows)
model1.save('./models/model_6_5')

model2 = BERTopic(language="english", calculate_probabilities=True, verbose=True, nr_topics=8, top_n_words=5)
topics2, probabilities2 = model2.fit_transform(rows)
model2.save('./models/model_8_5')

model3 = BERTopic(language="english", calculate_probabilities=True, verbose=True, nr_topics=8, top_n_words=10)
topics3, probabilities3 = model3.fit_transform(rows)
model3.save('./models/model_8_10')

model4 = BERTopic(language="english", calculate_probabilities=True, verbose=True, nr_topics=10, top_n_words=5)
topics4, probabilities4 = model4.fit_transform(rows)
model4.save('./models/model_10_5')

model5 = BERTopic(language="english", calculate_probabilities=True, verbose=True, nr_topics=10, top_n_words=10)
topics5, probabilities5 = model5.fit_transform(rows)
model5.save('./models/model_10_10')


