from google.cloud import language_v1
import json 

text = "OK, first I had that I had when I was to I say I had Gedda. I always loved European cars. And I always love that Jerry, very start the very good car with the family and sometimes my parents come from Italy, I needed an SUV saw before the disastrous side of GMC Acadia. And to me, it was just too overpriced for the product and the quality. I really wasn't happy with that. I as I said, I like European cars and I still like those bags. And it's very stardate good quality, not as expensive as the other competitors like. Mercedes, BMW, actually, to me, looks a little bit like the BMW, the exterior. And it's like car that has everything they wanted, like. With the touch screen, I can do the card play, yes, big space, a big trunk. I can even when I pull up to the seats, he has a good trunk space when I go shopping. It is spacious. The girls go out and on the bench in the back with their kids. I and just do that to sit in the back. And I feel like and I don't like the mix, like the two come and sit, but to me it's just wasting a spot there and it's like a car with six spaces, six seats. So I was like a big car. And all this space was to me it was just I didn't like that. So I like the bench, the seats. My seats get warm. I don't have the air conditioning, conditioning works really well, I just like the way he drives. I feel secure when I drive that car."


client = language_v1.LanguageServiceClient.from_service_account_json('../credentials.json')
type_ = language_v1.Document.Type.PLAIN_TEXT
encoding_type = language_v1.EncodingType.UTF8
language = "en"
    

def analyze_sentiment(text_content):
    result = {'text': text_content}

    document = {"content": text_content, "type_": type_, "language": language}

    # Available values: NONE, UTF8, UTF16, UTF32

    response = client.analyze_sentiment(request = {'document': document, 'encoding_type': encoding_type})
    # Get overall sentiment of the input document

    result['sentiment_score'] = response.document_sentiment.score
    result['sentiment_magnitude'] = response.document_sentiment.magnitude
    result['sentences'] = []        

    # Get sentiment for all sentences in the document
    for sentence in response.sentences:
        result['sentences'].append({'text':sentence.text.content, 'sentiment_score':sentence.sentiment.score, 'sentiment_magnitude':sentence.sentiment.magnitude})
 
    '''
    response = client.analyze_entity_sentiment(request = {'document': document, 'encoding_type': encoding_type})
    result['entities'] = []

    # Loop through entitites returned from the API
    for entity in response.entities:
        result['entities'].append({'name': entity.name, 'type': language_v1.Entity.Type(entity.type_).name, 'relevance_in_document': entity.salience, 'sentiment_score':entity.sentiment.score, 'sentiment_magnitude':entity.sentiment.magnitude})
    '''
    return result


def prepare_texts(data=[]):
    rows = []
    chat = ''
    dialogue = []
    id_counter = 1
    for row in data:
        if (row['Speaker'] == "Speaker 1" and chat.strip() != ""):
            rows.append({'id': row['filename'] + "-" + str(id_counter), 'chat': chat.strip(), 'dialogue': dialogue})
            id_counter = (id_counter + 1)
            chat = ''
            dialogue = []
        
        if ('Text' in row):
            chat = chat + row['Text'] + ' '
            dialogue.append({'speaker': row['Speaker'], 'text': row['Text']})

    return rows


def get_sentiments(data=[], speaker='Speaker 2'):
    result = []
    for idx in range(len(data)):
        i = data[idx]
        chat = ''
        for d in i['dialogue']:
            if (d['speaker'] == speaker):
                chat = chat + d['text'] + ' '
        s = analyze_sentiment(chat)
        result.append({'id': i['id'], 'sentiment': s})

    with open('./json/sentiment.json', 'w', encoding='utf-8') as f:
        json.dump(result, f, ensure_ascii=False, indent=4)

    return result



data = {}
with open('./json/transcript.json') as f:
    data = json.load(f)

texts = prepare_texts(data['transcript'])
sentiments = get_sentiments(texts)

# sample_analyze_sentiment(text)