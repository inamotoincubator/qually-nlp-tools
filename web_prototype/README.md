# JSON File generator for Qually

This folder contains files that will pre-process JSON files for the website prototype of Qually. Currently, the process is broken down into four separate Python scripts. The purpose of this is so that you can re-run pieces of the process without having to run the entire thing.

## `01_prepare_data.py`

This takes the raw transcripts, and converts them into two types of master JSON objects.
The first is `transcript.json` which is a vanilla conversion of the CSV files into JSON format. The second file is `conversation.json` which combines Speaker 1 and Speaker 2 into a single block of text for processing.

(Total script runtime: a few seconds)

## `02_create_models.py`

This script uses the BERTopic library to create and _save_ a model based on the documents you created in step 1. Currently, this script also generates 5 different models, each with different topic and word counts. The current settings are:

- 6 topics, 5 words per topic
- 8 topics, 5 words per topic
- 8 topics, 10 words per topic
- 10 topics, 5 words per topic
- 10 topics, 10 words per topic

The results of each model are saved to the `/models` folder. Note that the models aren't very big because they rely upon system level embeddings.

(Total runtime: 2 minutes per model, so currently 10 minutes)

## `03_generate_topics.py`

This script goes through each of the models we created in step 2 and generates a JSON file of individual topics and topic names. It also preserves topic ids from the model which can be used later for lookups.

(Total runtime: a few seconds)

## `04_match_topics.py`

The next step is to go back through the generated models and calculate the probability of a topic being relevant for each line of text. These topics vary per model, so we have to run each line of text through each individual model, so this is the most time consuming script. However, by calculating all of these variables at once, it allows us to easily save the output into `conversation-topics.json` and we can just render all the scenarios easily in the web interface.

(Total runtime: 2 conversations per second, so 600 conversations x 5 models = around 25 minutes)

## `05_cluster_models.py`

The last step is to go back through the topics and cluster them by models. This is a relatively straightforward exercise meant to generate JSON files that more closely represent the UI so you don't have to do this much computation in the browser.

This generates one JSON file _per model_ , e.g. model_6_5.json, etc.

NOTE: This script makes sure that only _one_ topic is assigned per conversation. Sometimes, the probability scores assign multiple topics to a piece of conversation, but we've scaled back on that in this script to keep the data sets smaller and more usable.

(Total runtime: 1 second)

## `06_get_sentiment.py`

This requires Google Cloud libraries
`pip install --upgrade google-cloud-language`
