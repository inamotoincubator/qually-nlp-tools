import spacy

nlp = spacy.load('bin/output/model-best')

text = ""
print("type : ‘quit’ to exit")

while text != "quit":
    text = input("Please enter example input: ")
    doc = nlp(text)
    print("Score on label: " + str(doc.cats['PURCHASE_BEHAVIOR']))

    if doc.cats['PURCHASE_BEHAVIOR'] > .5:
        print("This IS related to purchase behavior")
    else:
        print("This IS NOT related to purchase behavior")
